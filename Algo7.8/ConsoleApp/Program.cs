﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static string[] Cities = new string[19]
        {
            "Київ", //0
            "Житомир", //1
            "Новоград-Волинський", //2
            "Рівне", //3
            "Луцьк", //4
            "Бердичів", //5
            "Вінниця", //6
            "Хмельницький", //7
            "Тернопіль", //8
            "Шепетівка", //9
            "Біла церква", //10
            "Умань", //11
            "Черкаси", //12
            "Кременчуг", //13
            "Полтава", //14
            "Харків", //15
            "Прилуки", //16
            "Суми", //17
            "Миргород" //18
        };

        static int[,] graph = new int[19, 19];

        static void BFS()
        {
            int[] route = new int[20];

            bool[] visited = new bool[20];

            int cityIndex = 0;
            visited[cityIndex] = true;

            Queue<int> queue = new Queue<int>();
            queue.Enqueue(cityIndex);

            while (queue.Count != 0)
            {
                //Выносим со стека город, который будем проверять
                cityIndex = queue.Dequeue();
                for (int i = 0; i < Cities.Length; i++)
                {
                    //Если найден маршрут и мы ещё не были в этом городе
                    if (graph[cityIndex, i] != 0 && !visited[i])
                    {
                        //Отмечаем что были в этомм городе
                        visited[i] = true;
                        //Вносим в стек след город
                        queue.Enqueue(i);
                        //Считает километраж
                        route[i] = route[cityIndex] + graph[cityIndex, i];
                        Console.WriteLine($"Київ - {Cities[i]} = {route[i]}");
                    }
                }
            }
        }

        static int path = 0;

        static void DFS(int cityIndex, string route, int cityForOutput)
        {
            //Добавление городов
            if (cityIndex != 0)
            {
                route = $"{route} - {Cities[cityIndex]}";

                //Добавляем километраж
                path += graph[cityForOutput, cityIndex];
                Console.WriteLine($"{route} > {path}км");
            }
            //Начальный город
            else
            {
                route = $"{Cities[cityIndex]}";
            }

            //Рекурсия поиска в глубину
            for (int i = 0; i < Cities.Length; i++)
            {
                if (graph[cityIndex, i] != 0)
                {
                    DFS(i, route, cityIndex);
                    path -= graph[cityIndex, i];
                }
            }
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            graph[0, 1] = 135;
            graph[1, 2] = 80;
            graph[2, 3] = 100;
            graph[3, 4] = 68;
            graph[1, 5] = 38;
            graph[5, 6] = 73;
            graph[6, 7] = 110;
            graph[7, 8] = 104;
            graph[1, 9] = 115;
            graph[0, 10] = 78;
            graph[10, 11] = 115;
            graph[10, 12] = 146;
            graph[12, 13] = 105;
            graph[10, 14] = 181;
            graph[14, 15] = 130;
            graph[0, 16] = 128;
            graph[16, 17] = 175;
            graph[16, 18] = 109;

            Console.WriteLine("Матриця суміжності: \n");
            Console.Write("     ");
            for (int i = 0; i < graph.GetLength(0); i++)
            {
                Console.Write($"{i,5}");
            }
            Console.WriteLine("\n");
            for (int i = 0; i < graph.GetLength(0); i++)
            {
                Console.Write($"{i,5}");
                for (int j = 0; j < graph.GetLength(1); j++)
                {
                    Console.Write($"{graph[i, j],5}");
                }
                Console.WriteLine();
            }
            Console.WriteLine();

            Console.WriteLine("Пошук у ширину: \n");
            BFS();

            int cityIndex = 0;
            string route = "";
            Console.WriteLine();
            Console.WriteLine("Пошук у глибину: \n");
            DFS(cityIndex, route, 0);
        }
    }
}
