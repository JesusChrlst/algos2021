﻿using System;
using System.Text;
using System.Diagnostics;

namespace CountingSort
{
    class Program
    {
        public static void FindMax(short[] array, ref short max)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (max < array[i])
                {
                    max = array[i];
                }
            }
        }        
        public static void FindMin(short[] array, ref short min)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (min > array[i])
                {
                    min = array[i];
                }
            }
        }
        static void CountingSort(short[] array)
        {
            short min = array[0], max = array[0];
            FindMin(array, ref min);
            FindMax(array, ref max);

            //Длина масива - диапазон чисел
            short[] count = new short[max - min + 1];

            int j = 0;

            //Запоминаем на каких индексах у нас повторяются числа
            for (int i = 0; i < array.Length; i++)
            {
                count[array[i] - min]++;
            }

            //Если попадаемся на число, которое повторяется - записываем его в изначальный массив
            for (short i = min; i <= max; i++)
            {
                while (count[i - min]-- > 0)
                {
                    array[j] = i;
                    j++;
                }
            }
        }

        static void PrintArray(short[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine(arr[i]);
            }
            Console.WriteLine("");
        }

        public static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Stopwatch watch;

            Random rnd = new Random();

            Console.WriteLine("\n\tСортировка подсчётом, массив\n");

            short[] arr = new short[10];

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (short)rnd.Next(-10, 26);
            }

            CountingSort(arr);

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (short)rnd.Next(-10, 26);
            }

            watch = Stopwatch.StartNew();
            CountingSort(arr);
            watch.Stop();

            Console.WriteLine($"Сортировка для {arr.Length} элементов — {watch.Elapsed.TotalMilliseconds}ms");


            Array.Resize(ref arr, 100);

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (short)rnd.Next(-10, 26);
            }


            watch = Stopwatch.StartNew();
            CountingSort(arr);
            watch.Stop();

            Console.WriteLine($"Сортировка для {arr.Length} элементов — {watch.Elapsed.TotalMilliseconds}ms");


            Array.Resize(ref arr, 500);

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (short)rnd.Next(-10, 26);
            }

            watch = Stopwatch.StartNew();
            CountingSort(arr);
            watch.Stop();

            Console.WriteLine($"Сортировка для {arr.Length} элементов — {watch.Elapsed.TotalMilliseconds}ms");


            Array.Resize(ref arr, 1000);

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (short)rnd.Next(-10, 26);
            }

            watch = Stopwatch.StartNew();
            CountingSort(arr);
            watch.Stop();

            Console.WriteLine($"Сортировка для {arr.Length} элементов — {watch.Elapsed.TotalMilliseconds}ms");


            Array.Resize(ref arr, 2000);

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (short)rnd.Next(-10, 26);
            }

            watch = Stopwatch.StartNew();
            CountingSort(arr);
            watch.Stop();

            Console.WriteLine($"Сортировка для {arr.Length} элементов — {watch.Elapsed.TotalMilliseconds}ms");


            Array.Resize(ref arr, 5000);

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (short)rnd.Next(-10, 26);
            }

            watch = Stopwatch.StartNew();
            CountingSort(arr);
            watch.Stop();

            Console.WriteLine($"Сортировка для {arr.Length} элементов — {watch.Elapsed.TotalMilliseconds}ms");


            Array.Resize(ref arr, 10000);

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (short)rnd.Next(-10, 26);
            }

            watch = Stopwatch.StartNew();
            CountingSort(arr);
            watch.Stop();

            Console.WriteLine($"Сортировка для {arr.Length} элементов — {watch.Elapsed.TotalMilliseconds}ms");

        }
    }
}
