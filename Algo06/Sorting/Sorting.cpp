﻿#include <iostream>
#include <time.h>
#include <chrono>
#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>
#define RANGE 10

void HeapSort(double array[], int size)
{
	int i, j, subsidiary, heapRoot;
	double temp;

	for (i = 1; i < size; i++)
	{
		//индекс дочернего элемента равен i
		subsidiary = i;

		//выполнять пока индекс дочернего элемента не равен 0
		do
		{
			//коренной индекс = дочернему индексу(левому или правому, в зависимости от значений)
			heapRoot = (subsidiary - 1) / 2;

			//для максимального элемента в коренном индексе
			//если коренной элемент меньше дочернего
			if (array[heapRoot] < array[subsidiary])
			{
				//перезаписываем элементы
				temp = array[heapRoot];
				array[heapRoot] = array[subsidiary];
				array[subsidiary] = temp;
			}
			//printf("\n");

			//дочерний индекс равен коренному
			subsidiary = heapRoot;
		} while (subsidiary != 0);
	}

	//выполняем просев массива
	for (j = size - 1; j >= 0; j--)
	{
		//меняем местами 0 и элемент j, тем самым помещая элемент в конец списка для просева
		//j на каждой итерации -1
		temp = array[0];
		array[0] = array[j];
		array[j] = temp;
		//обнуление для просева
		heapRoot = 0;
		//выполняем действие пока дочерний не станет < j
		do
		{
			//ищем дочерний элемент
			/*индекс дочернего = 2 * индекс коренного + 1
			* (индекс дочернего = 2 * 0 + 1) = 1
			*/
			subsidiary = 2 * heapRoot + 1;
			//если дочерний меньше чем соседний дочерний и индекс дочернего < j - 1
			// < j - 1 для того, чтобы не трогать уже просеянные элементы
			if ((array[subsidiary] < array[subsidiary + 1]) && subsidiary < j - 1)
			{
				//проверяем соседний дочерний
				subsidiary++;
			}
			//если коренной элемент < дочернего(или соседнего дочернего) и индекс дочернего < j
			if (array[heapRoot] < array[subsidiary] && subsidiary < j)
			{
				temp = array[heapRoot];
				array[heapRoot] = array[subsidiary];
				array[subsidiary] = temp;
			}
			/*индекс коренного равен дочернему
			* например если проверяется 1 элемент, то коренной будет 0.
			* перезаписываем коренной на 1 и выполняем проверку для 3 и 4 элемента пока дочерний не упрётся в j
			*/
			heapRoot = subsidiary;
		} while (subsidiary < j);
	}
}

void ShellSort(float array[], int size)
{
	//Shell sort

	/*Например, у нас массив из 10 элементов.
	Сортировка Шелла подразумевает разбитие массива на 2 (10 / 2 = 5)
	Переносим элементы которые меньше влево
	(будет сравниваться 0 и 5 элемент, потом 1 и 6, потом 2 и 7 и т.д)
	После этого gap делим снова на 2 (5 / 2 = 2(округление)
	(будет сравниваться 0 и 2 элемент, 1 и 3, 2 и 4)
	И так до тех пор пока не будет полностью отсортирован массив
	*/

	int step = 1;
	int temp;
	int left, right;
	while (step <= size / 2)
		step = 2 * step + 1;

	//уменьшение расстояния
	for (step; step > 0; step = (step - 1) / 2)
	{
		for (right = step; right < size; right++)
		{
			temp = array[right];
			for (left = right; left >= step; left -= step)
			{
				if (temp < array[left - step])
				{
					array[left] = array[left - step];
				}
				else
				{
					break;
				}
			}
			array[left] = temp;
		}
	}
}

//Здесь должна была быть сортировка подсчётом, но на Сишке не получилось


void PrintArray(int array[], int size)
{
	for (int i = 0; i < size; i++)
	{
		printf("\n%d", array[i]);
	}
	printf("\n");
}

int main()
{
	system("chcp 1251");
	system("cls");

	srand(time(0));

	printf("\n\tПирамидальная сортировка, массив\n");

	double arrayMergeSort[11000];

	int size = 10;
	for (int i = 0; i < size; i++)
	{
		arrayMergeSort[i] = ((double)rand() / RAND_MAX * (100 + 20) - 20);
	}

	auto begin = GETTIME();
	HeapSort(arrayMergeSort, size);
	auto end = GETTIME();

	auto elapsed_ns = CALCTIME(end - begin);
	printf("\nВремя для %d элементов в наносекундах: %lldns\n", size, elapsed_ns.count());


	size = 100;
	for (int i = 0; i < size; i++)
	{
		arrayMergeSort[i] = ((double)rand() / RAND_MAX * (100 + 20) - 20);
	}

	begin = GETTIME();
	HeapSort(arrayMergeSort, size);
	end = GETTIME();

	elapsed_ns = CALCTIME(end - begin);
	printf("\nВремя для %d элементов в наносекундах: %lldns\n", size, elapsed_ns.count());


	size = 500;
	for (int i = 0; i < size; i++)
	{
		arrayMergeSort[i] = ((double)rand() / RAND_MAX * (100 + 20) - 20);
	}

	begin = GETTIME();
	HeapSort(arrayMergeSort, size);
	end = GETTIME();

	elapsed_ns = CALCTIME(end - begin);
	printf("\nВремя для %d элементов в наносекундах: %lldns\n", size, elapsed_ns.count());


	size = 1000;
	for (int i = 0; i < size; i++)
	{
		arrayMergeSort[i] = ((double)rand() / RAND_MAX * (100 + 20) - 20);
	}

	begin = GETTIME();
	HeapSort(arrayMergeSort, size);
	end = GETTIME();

	elapsed_ns = CALCTIME(end - begin);
	printf("\nВремя для %d элементов в наносекундах: %lldns\n", size, elapsed_ns.count());


	size = 2000;
	for (int i = 0; i < size; i++)
	{
		arrayMergeSort[i] = ((double)rand() / RAND_MAX * (100 + 20) - 20);
	}

	begin = GETTIME();
	HeapSort(arrayMergeSort, size);
	end = GETTIME();

	elapsed_ns = CALCTIME(end - begin);
	printf("\nВремя для %d элементов в наносекундах: %lldns\n", size, elapsed_ns.count());


	size = 5000;
	for (int i = 0; i < size; i++)
	{
		arrayMergeSort[i] = ((double)rand() / RAND_MAX * (100 + 20) - 20);
	}

	begin = GETTIME();
	HeapSort(arrayMergeSort, size);
	end = GETTIME();

	elapsed_ns = CALCTIME(end - begin);
	printf("\nВремя для %d элементов в наносекундах: %lldns\n", size, elapsed_ns.count());


	printf("\n\n\tСортировка Шелла, массив\n");

	float arrayShellSort[11000];

	size = 10;
	for (int i = 0; i < size; i++)
	{
		arrayShellSort[i] = ((float)rand() / RAND_MAX * (10 - 10 + 1) + 10);
	}

	begin = GETTIME();
	ShellSort(arrayShellSort, size);
	end = GETTIME();

	elapsed_ns = CALCTIME(end - begin);
	printf("\nВремя для %d элементов в наносекундах: %lldns\n", size, elapsed_ns.count());


	size = 100;
	for (int i = 0; i < size; i++)
	{
		arrayShellSort[i] = ((float)rand() / RAND_MAX * (10 - 10 + 1) + 10);
	}

	begin = GETTIME();
	ShellSort(arrayShellSort, size);
	end = GETTIME();

	elapsed_ns = CALCTIME(end - begin);
	printf("\nВремя для %d элементов в наносекундах: %lldns\n", size, elapsed_ns.count());


	size = 500;
	for (int i = 0; i < size; i++)
	{
		arrayShellSort[i] = ((float)rand() / RAND_MAX * (10 - 10 + 1) + 10);
	}

	begin = GETTIME();
	ShellSort(arrayShellSort, size);
	end = GETTIME();

	elapsed_ns = CALCTIME(end - begin);
	printf("\nВремя для %d элементов в наносекундах: %lldns\n", size, elapsed_ns.count());


	size = 1000;
	for (int i = 0; i < size; i++)
	{
		arrayShellSort[i] = ((float)rand() / RAND_MAX * (10 - 10 + 1) + 10);
	}

	begin = GETTIME();
	ShellSort(arrayShellSort, size);
	end = GETTIME();

	elapsed_ns = CALCTIME(end - begin);
	printf("\nВремя для %d элементов в наносекундах: %lldns\n", size, elapsed_ns.count());


	size = 2000;
	for (int i = 0; i < size; i++)
	{
		arrayShellSort[i] = ((float)rand() / RAND_MAX * (10 - 10 + 1) + 10);
	}

	begin = GETTIME();
	ShellSort(arrayShellSort, size);
	end = GETTIME();

	elapsed_ns = CALCTIME(end - begin);
	printf("\nВремя для %d элементов в наносекундах: %lldns\n", size, elapsed_ns.count());


	size = 5000;
	for (int i = 0; i < size; i++)
	{
		arrayShellSort[i] = ((float)rand() / RAND_MAX * (10 - 10 + 1) + 10);
	}

	begin = GETTIME();
	ShellSort(arrayShellSort, size);
	end = GETTIME();

	elapsed_ns = CALCTIME(end - begin);
	printf("\nВремя для %d элементов в наносекундах: %lldns\n", size, elapsed_ns.count());


	size = 10000;
	for (int i = 0; i < size; i++)
	{
		arrayShellSort[i] = ((float)rand() / RAND_MAX * (10 - 10 + 1) + 10);
	}

	begin = GETTIME();
	ShellSort(arrayShellSort, size);
	end = GETTIME();

	elapsed_ns = CALCTIME(end - begin);
	printf("\nВремя для %d элементов в наносекундах: %lldns\n", size, elapsed_ns.count());

	return 0;
}
