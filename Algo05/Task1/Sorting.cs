﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;

namespace Task1
{
    class Sorting
    {
        static void selectionSortList(LinkedListNode<int> head)
        {
            LinkedListNode<int> temp = head;

            //Проходимся по списку
            while (temp != null)
            {
                LinkedListNode<int> min = temp;
                LinkedListNode<int> after = temp.Next;
                //Проход по несортированому списку
                while (after != null)
                {
                    //Ищем минимальный
                    if (min.Value > after.Value)
                    {
                        min = after;
                    }
                    after = after.Next;
                }
                //Обмениваем элементы
                int tempSort = temp.Value;
                temp.Value = min.Value;
                min.Value = tempSort;
                temp = temp.Next;
            }
        }

        static void insertionSortArray(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                int j;
                int temp = array[i];
                //Вставляем элементы в отсортированую часть массива
                for (j = i - 1; j >= 0; j--)
                {
                    if (array[j] < temp)
                    {
                        break;
                    }
                    //Если элемент меньше чем проверяемый то перетаскиваем его вниз
                    array[j + 1] = array[j];
                }
                //Ставим элемент на своё место
                array[j + 1] = temp;
            }
        }

        public static void InsertionSortList(LinkedList<int> list)
        {
            LinkedListNode<int> current = list.First;
            LinkedListNode<int> nodeSorting;
            LinkedListNode<int> nodePlace;
            int temp;

            while (current != null)
            {
                //Ищем минимум
                if (current.Previous == null)
                {
                    current = current.Next;
                }
                //Сохраняем адрес текущего элемента
                nodeSorting = current;
                //Переменная для сохранения предыдущего адреса
                nodePlace = nodeSorting.Previous;
                //Пока не упрёмся в начало и сортируемый < предыдущего
                while (nodePlace != null && nodeSorting.Value < nodePlace.Value)
                {
                    //Swap
                    temp = nodePlace.Value;
                    nodePlace.Value = nodeSorting.Value;
                    nodeSorting.Value = temp;

                    //Запоминаем адрес где сейчас проверяемый элемент(пред. узел)
                    nodeSorting = nodePlace;
                    nodePlace = nodePlace.Previous;
                }

                current = current.Next;
            }
            //Если вкратце, если текущий элемент меньше предыдущего, свапаем их значения и адреса, после цикла переходим на след. елемент
        }
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            LinkedListNode<int> listNode = new LinkedListNode<int>(1);
            LinkedList<int> list = new LinkedList<int>();
            list.AddLast(listNode);


            Stopwatch watch;

            Random rnd = new Random();

            Console.WriteLine("\nСортировка выбором двусвязный список\n");

            for (int i = 0; i < 10; i++)
            {
                int random = rnd.Next(0, 50);
                list.AddLast(random);
            }

            selectionSortList(listNode);

            for (int i = 0; i < 10; i++)
            {
                int random = rnd.Next(0, 50);
                list.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            selectionSortList(listNode);
            watch.Stop();

            Console.WriteLine($"Соритровка для 10 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            for (int i = 0; i < 100; i++)
            {
                int random = rnd.Next(0, 50);
                list.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            selectionSortList(listNode);
            watch.Stop();

            Console.WriteLine($"Соритровка для 100 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            for (int i = 0; i < 500; i++)
            {
                int random = rnd.Next(0, 50);
                list.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            selectionSortList(listNode);
            watch.Stop();

            Console.WriteLine($"Соритровка для 500 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            for (int i = 0; i < 1000; i++)
            {
                int random = rnd.Next(0, 50);
                list.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            selectionSortList(listNode);
            watch.Stop();

            Console.WriteLine($"Соритровка для 1000 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            for (int i = 0; i < 2000; i++)
            {
                int random = rnd.Next(0, 50);
                list.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            selectionSortList(listNode);
            watch.Stop();

            Console.WriteLine($"Соритровка для 2000 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            for (int i = 0; i < 5000; i++)
            {
                int random = rnd.Next(0, 50);
                list.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            selectionSortList(listNode);
            watch.Stop();

            Console.WriteLine($"Соритровка для 5000 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            for (int i = 0; i < 10000; i++)
            {
                int random = rnd.Next(0, 50);
                list.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            selectionSortList(listNode);
            watch.Stop();

            Console.WriteLine($"Соритровка для 10000 элементов — {watch.Elapsed.TotalMilliseconds}ms");


            Console.WriteLine("\nСортировка вставками массив\n");

            int[] array = new int[10];

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(0, 50);
            }
            insertionSortArray(array);

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(0, 50);
            }

            watch = Stopwatch.StartNew();
            insertionSortArray(array);
            watch.Stop();

            Console.WriteLine($"Сортировка для 10 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            Array.Resize(ref array, 100);

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(0, 50);
            }

            watch = Stopwatch.StartNew();
            insertionSortArray(array);
            watch.Stop();

            Console.WriteLine($"Сортировка для 100 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            Array.Resize(ref array, 500);

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(0, 50);
            }

            watch = Stopwatch.StartNew();
            insertionSortArray(array);
            watch.Stop();

            Console.WriteLine($"Сортировка для 500 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            Array.Resize(ref array, 1000);

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(0, 50);
            }

            watch = Stopwatch.StartNew();
            insertionSortArray(array);
            watch.Stop();

            Console.WriteLine($"Сортировка для 1000 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            Array.Resize(ref array, 2000);

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(0, 50);
            }

            watch = Stopwatch.StartNew();
            insertionSortArray(array);
            watch.Stop();

            Console.WriteLine($"Сортировка для 2000 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            Array.Resize(ref array, 5000);

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(0, 50);
            }

            watch = Stopwatch.StartNew();
            insertionSortArray(array);
            watch.Stop();

            Console.WriteLine($"Сортировка для 5000 элементов — {watch.Elapsed.TotalMilliseconds}ms");

            Array.Resize(ref array, 10000);

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(0, 50);
            }

            watch = Stopwatch.StartNew();
            insertionSortArray(array);
            watch.Stop();

            Console.WriteLine($"Сортировка для 10000 элементов — {watch.Elapsed.TotalMilliseconds}ms");


            Console.WriteLine("\nСортировка вставками двусвязный список\n");

            LinkedListNode<int> listInsertionNode = new LinkedListNode<int>(1);
            LinkedList<int> listInsertion = new LinkedList<int>();
            list.AddLast(listInsertionNode);

            int size = 10;
            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(0, 50);
                listInsertion.AddLast(random);
            }

            InsertionSortList(listInsertion);


            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(0, 50);
                listInsertion.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            InsertionSortList(listInsertion);
            watch.Stop();

            Console.WriteLine($"Сортировка для {size} элементов — {watch.Elapsed.TotalMilliseconds}ms");

            size = 100;
            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(0, 50);
                listInsertion.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            InsertionSortList(listInsertion);
            watch.Stop();

            Console.WriteLine($"Сортировка для {size} элементов — {watch.Elapsed.TotalMilliseconds}ms");

            size = 500;
            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(0, 50);
                listInsertion.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            InsertionSortList(listInsertion);
            watch.Stop();

            Console.WriteLine($"Сортировка для {size} элементов — {watch.Elapsed.TotalMilliseconds}ms");

            size = 1000;
            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(0, 50);
                listInsertion.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            InsertionSortList(listInsertion);
            watch.Stop();

            Console.WriteLine($"Сортировка для {size} элементов — {watch.Elapsed.TotalMilliseconds}ms");

            size = 2000;
            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(0, 50);
                listInsertion.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            InsertionSortList(listInsertion);
            watch.Stop();

            Console.WriteLine($"Сортировка для {size} элементов — {watch.Elapsed.TotalMilliseconds}ms");

            size = 5000;
            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(0, 50);
                listInsertion.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            InsertionSortList(listInsertion);
            watch.Stop();

            Console.WriteLine($"Сортировка для {size} элементов — {watch.Elapsed.TotalMilliseconds}ms");

            size = 10000;
            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(0, 50);
                listInsertion.AddLast(random);
            }

            watch = Stopwatch.StartNew();
            InsertionSortList(listInsertion);
            watch.Stop();

            Console.WriteLine($"Сортировка для {size} элементов — {watch.Elapsed.TotalMilliseconds}ms");
        }
    }
}

