﻿using System;
using System.Diagnostics;
using System.Text;

namespace Task2_10_
{
    class Program
    {
        private static void BubbleSort(int[] arr)
        {
            for (int i = 1; i < arr.Length; i++)
            {
                for (int j = 1; j < arr.Length; j++)
                {
                    if (arr[j - 1] > arr[j])
                    {
                        int temp = arr[j - 1];
                        arr[j - 1] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            int arraySize = 1023;

            int[] arr = new int[arraySize];

            Random rnd = new Random();

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(0, 30);
            }

            System.Diagnostics.Stopwatch sw = new Stopwatch();
            sw.Start();

            BubbleSort(arr);

            sw.Stop();

            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine($"[{i}] - {arr[i]}");
            }

            Console.WriteLine($"\nЧас виконання функції — {sw.Elapsed}");
        }
    }
}
