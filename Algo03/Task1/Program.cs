﻿using System;
using System.Text;

namespace lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            int i, menu;

            Console.WriteLine($"Оберіть функцію:\n1) f(n)=n\n2) f(n)=log(n)\n3) f(n)=n*log(n)\n4) f(n)=n{(char)0xB2}\n5) f(n)=2{(char)0x207F}\n6) f(n)=n!\n");
            menu = int.Parse(Console.ReadLine());

            double number;

            switch (menu)
            {
                case 1:
                    for (i = 0; i <= 50; i++)
                    {
                        number = i;
                        Console.WriteLine($"n = {i}, f(n) = {number}");
                    }
                    break;

                case 2:
                    for (i = 1; i <= 50; i++)
                    {
                        number = Math.Log(i);
                        Console.WriteLine($"n = {i}, f(n) = {number:F2}");
                    }
                    break;

                case 3:
                    for (i = 1; i <= 50; i++)
                    {
                        number = i * Math.Log(i);
                        Console.WriteLine($"n = {i}, f(n) = {number:F2}");
                    }
                    break;

                case 4:
                    for (i = 0; i <= 50; i++)
                    {

                        number = Math.Pow(i, 2);
                        if (number > 500)
                        {
                            break;
                        }
                        Console.WriteLine($"n = {i}, f(n) = {number}");
                    }
                    break;

                case 5:
                    for (i = 0; i <= 50; i++)
                    {
                        number = Math.Pow(2, i);
                        if (number > 500)
                        {
                            break;
                        }
                        Console.WriteLine($"n = {i}, f(n) = {number}");  
                    }
                    break;

                case 6:
                    for (i = 0; i <= 50; i++)
                    {
                        int factorial = 1;
                        for (int j = 1; j <= i; j++)
                        {
                            factorial *= j;
                        }

                        number = factorial;

                        if (number > 500)
                        {
                            break;
                        }
                        Console.WriteLine($"n = {i}, f(n) = {number}");
                    }
                    break;

                default:
                    {
                        Console.WriteLine("Неправильно обрано пункт меню!");
                    }
                    break;
            }
        }
    }
}