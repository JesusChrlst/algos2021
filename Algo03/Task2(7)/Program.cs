﻿using System;
using System.Diagnostics;
using System.Text;

namespace Task2_7_
{
    class Program
    {
        private static double BinToDec(double[] arr)
        {
            double res = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                res += arr[i] * Math.Pow(2, arr.Length - 1 - i);
            }
            return res;
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            double[] arr = new double[40];

            Random rnd = new Random();

            int numberOne = 0;
            Console.Write("\n0b");
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(0, 2);
                if (arr[i] == 1)
                {
                    numberOne++;
                }
                Console.Write($"{arr[i]}");

            }

            Console.WriteLine($"\n\nУ десятковій системі числення — {BinToDec(arr)}\n\n");


            Console.Write("0b");
            for (int i = 0; i < arr.Length; i++)
            {
                if (i < numberOne)
                {
                    arr[i] = 1;
                }
                else
                {
                    arr[i] = 0;
                }
                Console.Write($"{arr[i]}");
            }

            System.Diagnostics.Stopwatch sw = new Stopwatch();
            sw.Start();

            double result = BinToDec(arr);

            sw.Stop();

            Console.WriteLine($"\n\nУ десятковій системі числення — {result}");
            Console.WriteLine($"\n\nЧас виконання — {sw.Elapsed}");

        }
    }
}
