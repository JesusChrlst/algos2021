﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>

long long MyRandomizer(long long seed = 0)
{
	int a = 48271;
	char c = 0;
	long long m = (1LL << 31) - 1;
	static long long previousX;

	if (seed != 0)
	{
		previousX = seed;
	}

	long long x = (a * previousX + c) % m;
	previousX = x;
	return x;
}

double Dyspersion(int* arr, double mathExp)
{
	double dyspersion = 0;
	double probability = 0;

	int count = 0;
	for (int i = 0; i < 50000; i++)
	{
		if (arr[i] == arr[i + 1])
		{
			count++;
		}
		else
		{
			probability = count / 50000.0;
			dyspersion += pow((arr[i] - mathExp), 2) * probability;
		}
	}

	return dyspersion;
}

int main()
{
	system("chcp 1251");
	system("cls");

	int begin = 0;
	int finish = 300;
	int arr[50000];
	int iterations = 50000;

	MyRandomizer(time(0));
	for (int i = 0; i < iterations; i++)
	{
		long long var = MyRandomizer() % (finish - begin) + begin;
		arr[i] = var;
	}


	for (int i = 0; i < 50000 - 1; i++)
	{
		for (int j = (50000 - 1); j > i; j--)
		{
			if (arr[j - 1] > arr[j])
			{
				int temp = arr[j - 1];
				arr[j - 1] = arr[j];
				arr[j] = temp;
			}
		}
	}

	double mathExp = 0;
	double dyspersion = 0;
	double probability = 0;

	int count = 0;
	for (int i = 0; i < 50000; i++)
	{
		if (arr[i] == arr[i + 1])
		{
			count++;
		}
		else
		{
			probability = count / 50000.0;
			printf("Число: %d; Зустрічається: %d; Імовірність появи: %f\n", arr[i], count, probability);

			mathExp += arr[i] * probability;
			count = 0;
		}
	}

	dyspersion = Dyspersion(arr, mathExp);

	printf("\nМатематичне сподівання: %f\n", mathExp);
	printf("\nДисперсія: %f\n", dyspersion);
	printf("\nСередньоквадратичне відхилення: %f\n", sqrt(dyspersion));



	return 0;
}

