﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <time.h>

struct CurrentDate
{
	unsigned short Day : 5;
	unsigned short Month : 4;
	unsigned short Year : 7;
	unsigned short Hour : 5;
	unsigned short Minute : 6;
	unsigned short Second : 6;
};


int main()
{
	//
	system("chcp 1251");
	system("cls");

	long long systemTime;
	struct tm* m_time;

	systemTime = time(NULL);
	m_time = localtime(&systemTime);
	char stringTime[128] = "";
	strftime(stringTime, 128, "%x", m_time);

	CurrentDate date;

	char* pointer;
	pointer = strtok(stringTime, "/");
	date.Month = atoi(pointer);
	pointer = strtok(NULL, "/");
	date.Day = atoi(pointer);
	pointer = strtok(NULL, "/");
	date.Year = atoi(pointer);

	strftime(stringTime, 128, "%X", m_time);
	pointer = strtok(stringTime, ":");
	date.Hour = atoi(pointer);
	pointer = strtok(NULL, ":");
	date.Minute = atoi(pointer);
	pointer = strtok(NULL, ":");
	date.Second = atoi(pointer);

	printf("%d.%d.%d %d:%d:%d", date.Day, date.Month, date.Year, date.Hour, date.Minute, date.Second);

	time_t seconds = time(NULL);
	tm* timeinfo = localtime(&seconds);


	printf("\n\nВласна структура: %d байти\tІнтегрована: %d байт\n", sizeof(date), sizeof(tm));
	return 0;
}