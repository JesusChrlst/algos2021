﻿#include <iostream>

union SignedChar
{
	signed char x;
	struct
	{
		unsigned char b0 : 1;
		unsigned char b1 : 1;
		unsigned char b2 : 1;
		unsigned char b3 : 1;
		unsigned char b4 : 1;
		unsigned char b5 : 1;
		unsigned char b6 : 1;
		unsigned char b7 : 1;

	}uc;
};


int main()
{
	system("chcp 1251");
	system("cls");

	signed char num1, num2, result;

	num1 = 5;
	num2 = 127;
	result = num1 + num2;
	printf("result: %d + %d = %d", num1, num2, result);

	SignedChar uchar;

	uchar.x = num1;
	printf("\n%d = 0b%d%d%d%d%d%d%d%d", uchar.x, uchar.uc.b7, uchar.uc.b6, uchar.uc.b5, uchar.uc.b4, uchar.uc.b3, uchar.uc.b2, uchar.uc.b1, uchar.uc.b0);
	uchar.x = num2;
	printf("\n%d = 0b%d%d%d%d%d%d%d%d", uchar.x, uchar.uc.b7, uchar.uc.b6, uchar.uc.b5, uchar.uc.b4, uchar.uc.b3, uchar.uc.b2, uchar.uc.b1, uchar.uc.b0);
	uchar.x = result;
	printf("\n%d = 0b%d%d%d%d%d%d%d%d", uchar.x, uchar.uc.b7, uchar.uc.b6, uchar.uc.b5, uchar.uc.b4, uchar.uc.b3, uchar.uc.b2, uchar.uc.b1, uchar.uc.b0);

	num1 = 2;
	num2 = 3;
	result = num1 - num2;
	printf("\n\n\nresult: %d - %d = %d", num1, num2, result);

	uchar.x = num1;
	printf("\n%d = 0b%d%d%d%d%d%d%d%d", uchar.x, uchar.uc.b7, uchar.uc.b6, uchar.uc.b5, uchar.uc.b4, uchar.uc.b3, uchar.uc.b2, uchar.uc.b1, uchar.uc.b0);
	uchar.x = num2;
	printf("\n%d = 0b%d%d%d%d%d%d%d%d", uchar.x, uchar.uc.b7, uchar.uc.b6, uchar.uc.b5, uchar.uc.b4, uchar.uc.b3, uchar.uc.b2, uchar.uc.b1, uchar.uc.b0);
	uchar.x = result;
	printf("\n%d = 0b%d%d%d%d%d%d%d%d", uchar.x, uchar.uc.b7, uchar.uc.b6, uchar.uc.b5, uchar.uc.b4, uchar.uc.b3, uchar.uc.b2, uchar.uc.b1, uchar.uc.b0);

	num1 = -120;
	num2 = 34;
	result = num1 - num2;
	printf("\n\n\nresult: %d - %d = %d", num1, num2, result);

	uchar.x = num1;
	printf("\n%d = 0b%d%d%d%d%d%d%d%d", uchar.x, uchar.uc.b7, uchar.uc.b6, uchar.uc.b5, uchar.uc.b4, uchar.uc.b3, uchar.uc.b2, uchar.uc.b1, uchar.uc.b0);
	uchar.x = num2;
	printf("\n%d = 0b%d%d%d%d%d%d%d%d", uchar.x, uchar.uc.b7, uchar.uc.b6, uchar.uc.b5, uchar.uc.b4, uchar.uc.b3, uchar.uc.b2, uchar.uc.b1, uchar.uc.b0);
	uchar.x = result;
	printf("\n%d = 0b%d%d%d%d%d%d%d%d", uchar.x, uchar.uc.b7, uchar.uc.b6, uchar.uc.b5, uchar.uc.b4, uchar.uc.b3, uchar.uc.b2, uchar.uc.b1, uchar.uc.b0);

	unsigned char c = -5;
	uchar.x = c;
	printf("\n\n%d = %d = 0b%d%d%d%d%d%d%d%d", uchar.x, c, uchar.uc.b7, uchar.uc.b6, uchar.uc.b5, uchar.uc.b4, uchar.uc.b3, uchar.uc.b2, uchar.uc.b1, uchar.uc.b0);


	num1 = 56;
	num2 = 38;
	result = num1 & num2;
	printf("\n\n\nresult: %d & %d = %d", num1, num2, result);

	uchar.x = num1;
	printf("\n%d = 0b%d%d%d%d%d%d%d%d", uchar.x, uchar.uc.b7, uchar.uc.b6, uchar.uc.b5, uchar.uc.b4, uchar.uc.b3, uchar.uc.b2, uchar.uc.b1, uchar.uc.b0);
	uchar.x = num2;
	printf("\n%d = 0b%d%d%d%d%d%d%d%d", uchar.x, uchar.uc.b7, uchar.uc.b6, uchar.uc.b5, uchar.uc.b4, uchar.uc.b3, uchar.uc.b2, uchar.uc.b1, uchar.uc.b0);
	uchar.x = result;
	printf("\n%d = 0b%d%d%d%d%d%d%d%d", uchar.x, uchar.uc.b7, uchar.uc.b6, uchar.uc.b5, uchar.uc.b4, uchar.uc.b3, uchar.uc.b2, uchar.uc.b1, uchar.uc.b0);

	num1 = 56;
	num2 = 38;
	result = num1 | num2;
	printf("\n\n\nresult: %d | %d = %d", num1, num2, result);

	uchar.x = num1;
	printf("\n%d = 0b%d%d%d%d%d%d%d%d", uchar.x, uchar.uc.b7, uchar.uc.b6, uchar.uc.b5, uchar.uc.b4, uchar.uc.b3, uchar.uc.b2, uchar.uc.b1, uchar.uc.b0);
	uchar.x = num2;
	printf("\n%d = 0b%d%d%d%d%d%d%d%d", uchar.x, uchar.uc.b7, uchar.uc.b6, uchar.uc.b5, uchar.uc.b4, uchar.uc.b3, uchar.uc.b2, uchar.uc.b1, uchar.uc.b0);
	uchar.x = result;
	printf("\n%d = 0b%d%d%d%d%d%d%d%d", uchar.x, uchar.uc.b7, uchar.uc.b6, uchar.uc.b5, uchar.uc.b4, uchar.uc.b3, uchar.uc.b2, uchar.uc.b1, uchar.uc.b0);


	return 0;
}
