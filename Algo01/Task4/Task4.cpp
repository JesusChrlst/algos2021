﻿#include <iostream>
#include <windows.h>
#include <time.h>
#include <string.h>

union Float
{

	float num;
	struct
	{
		unsigned char b31 : 1;
	}sign;
	struct
	{
		unsigned char b23 : 1;
		unsigned char b24 : 1;
		unsigned char b25 : 1;
		unsigned char b26 : 1;
		unsigned char b27 : 1;
		unsigned char b28 : 1;
		unsigned char b29 : 1;
		unsigned char b30 : 1;
	}degree;
	struct
	{
		unsigned char b0 : 1;
		unsigned char b1 : 1;
		unsigned char b2 : 1;
		unsigned char b3 : 1;
		unsigned char b4 : 1;
		unsigned char b5 : 1;
		unsigned char b6 : 1;
		unsigned char b7 : 1;
		unsigned char b8 : 1;
		unsigned char b9 : 1;
		unsigned char b10 : 1;
		unsigned char b11 : 1;
		unsigned char b12 : 1;
		unsigned char b13 : 1;
		unsigned char b14 : 1;
		unsigned char b15 : 1;
		unsigned char b16 : 1;
		unsigned char b17 : 1;
		unsigned char b18 : 1;
		unsigned char b19 : 1;
		unsigned char b20 : 1;
		unsigned char b21 : 1;
		unsigned char b22 : 1;
	}mantissa;
};


int main()
{
	system("chcp 1251");
	system("cls");

	float f = 3156.00153;

	Float example;
	example.num = f;

	printf("Число %f\n\n", f);
	printf("Значення побітово = 0b%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n\n", example.sign.b31, example.degree.b30, example.degree.b29, example.degree.b28, example.degree.b27, example.degree.b26, example.degree.b25, example.degree.b24, example.degree.b23, example.mantissa.b22, example.mantissa.b21, example.mantissa.b20, example.mantissa.b19, example.mantissa.b18, example.mantissa.b17, example.mantissa.b16, example.mantissa.b15, example.mantissa.b14, example.mantissa.b13, example.mantissa.b12, example.mantissa.b11, example.mantissa.b10, example.mantissa.b9, example.mantissa.b8, example.mantissa.b7, example.mantissa.b6, example.mantissa.b5, example.mantissa.b4, example.mantissa.b3, example.mantissa.b2, example.mantissa.b1, example.mantissa.b0);

	return 0;
}
