﻿#include <iostream>
#include <windows.h>
#include <time.h>
#include <string.h>

union SigShort {

	signed short num;
	struct
	{
		struct
		{
			unsigned char b0 : 1;
			unsigned char b1 : 1;
			unsigned char b2 : 1;
			unsigned char b3 : 1;
			unsigned char b4 : 1;
			unsigned char b5 : 1;
			unsigned char b6 : 1;
			unsigned char b7 : 1;

		}byte0sh;

		struct
		{
			unsigned char b8 : 1;
			unsigned char b9 : 1;
			unsigned char b10 : 1;
			unsigned char b11 : 1;
			unsigned char b12 : 1;
			unsigned char b13 : 1;
			unsigned char b14 : 1;
			unsigned char b15 : 1;

		}byte1sh;

	}bytes;
};

int main()
{
	system("chcp 1251");
	system("cls");

	SigShort sh;

	sh.num = -69;

	printf("\nЧисло %d = 0b%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n\n", sh.num, sh.bytes.byte1sh.b15, sh.bytes.byte1sh.b14, sh.bytes.byte1sh.b13, sh.bytes.byte1sh.b12, sh.bytes.byte1sh.b11, sh.bytes.byte1sh.b10, sh.bytes.byte1sh.b9, sh.bytes.byte1sh.b8, sh.bytes.byte0sh.b7, sh.bytes.byte0sh.b6, sh.bytes.byte0sh.b5, sh.bytes.byte0sh.b4, sh.bytes.byte0sh.b3, sh.bytes.byte0sh.b2, sh.bytes.byte0sh.b1, sh.bytes.byte0sh.b0);

	if (sh.bytes.byte1sh.b15 ^ 0 == 0)
	{
		if (sh.num == 0)
		{
			printf("Число = 0");
		}
		else
		{
			printf("Число додатне\n");
		}
	}
	else
	{
		printf("Число від'ємне\n");
	}


	sh.num = 38;

	printf("\nЧисло %d = 0b%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n\n", sh.num, sh.bytes.byte1sh.b15, sh.bytes.byte1sh.b14, sh.bytes.byte1sh.b13, sh.bytes.byte1sh.b12, sh.bytes.byte1sh.b11, sh.bytes.byte1sh.b10, sh.bytes.byte1sh.b9, sh.bytes.byte1sh.b8, sh.bytes.byte0sh.b7, sh.bytes.byte0sh.b6, sh.bytes.byte0sh.b5, sh.bytes.byte0sh.b4, sh.bytes.byte0sh.b3, sh.bytes.byte0sh.b2, sh.bytes.byte0sh.b1, sh.bytes.byte0sh.b0);

	if (sh.bytes.byte1sh.b15 ^ 0 == 0)
	{
		if (sh.num == 0)
		{
			printf("Число = 0");
		}
		else
		{
			printf("Число додатне\n");
		}
	}
	else
	{
		printf("Число від'ємне\n");
	}

	return 0;
}
