﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task2
{
    class Program
    {
        static void CheckInt(ref int number)
        {
            while (true)
            {
                if (int.TryParse(Console.ReadLine(), out number))
                {
                    break;
                }
                Console.WriteLine("Помилка введення значення");
            }

        }

        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;

            Console.WriteLine("\n\n\t\t----//Створення списку//----\n\n");

            LinkedList<int> list = new LinkedList<int>();

            Console.Write("Введіть кількість початкових елементів: ");
            int begin = 0;
            CheckInt(ref begin);

            int num = 0;
            Console.WriteLine("\nЗаповніть список: \n");
            for (int i = 0; i < begin; i++)
            {
                Console.Write($"Число №{i} = ");
                CheckInt(ref num);

                list.AddLast(num);
            }

            int menu = 0;

            do
            {
                Console.WriteLine("\n\n1. Додати елемент до початку списку");
                Console.WriteLine("2. Видалити елемент з початку списку");
                Console.WriteLine("3. Додати елемент до кінця списку");
                Console.WriteLine("4. Видалити елемент з кінця списку");
                Console.WriteLine("5. Вивести список");
                Console.WriteLine("6. Знищити список");
                Console.WriteLine("0. Вихід з програми");
                Console.Write("Оберіть дію: ");
                CheckInt(ref menu);

                switch(menu)
                {
                    case 0:
                        {
                            break;
                        }
                    case 1:
                        {
                            Console.Write("\nВведіть значення елемента: ");
                            int elem = 0;
                            CheckInt(ref elem);
                            list.AddFirst(elem);
                            Console.WriteLine("\nЕлемент додано в початок списку");
                            break;
                        }                    
                    case 2:
                        {
                            Console.WriteLine("\nПерший елемент видалено");
                            list.RemoveFirst();
                            break;
                        }
                    case 3:
                        {
                            Console.Write("\nВведіть значення елемента: ");
                            int elem = 0;
                            CheckInt(ref elem);
                            list.AddLast(elem);
                            Console.WriteLine("\nЕлемент додано в кінець списку");
                            break;
                        }
                    case 4:
                        {
                            Console.WriteLine("\nОстанній елемент видалено");
                            list.RemoveLast();
                            break;
                        }
                    case 5:
                        {
                            Console.WriteLine("\n");
                            if (list.First == null)
                            {
                                Console.WriteLine("Немає елементів у списку");
                            }
                            int i = 0;
                            foreach(var element in list)
                            {
                                Console.WriteLine($"[{i}] = {element}");
                                i++;
                            }
                            break;
                        }
                    case 6:
                        {
                            list.Clear();
                            Console.WriteLine("\nСпиок очищено");
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("\n\nНема такого пункту меню!\n\n");
                            break;
                        }

                }

            } while (menu != 0);
        }
    }
}
