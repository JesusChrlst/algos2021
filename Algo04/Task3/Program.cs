﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Task3
{
    class Program
    {
        public static List<string> RPN(string expression)
        {
            List<string> reversePolishNotation = new List<string>();
            Stack<string> stack = new Stack<string>();

            string[] expressionArr = expression.Split(new char[2] { ' ', '\n' });
            double num;

            foreach (string item in expressionArr)
            {
                // Цифра
                if (double.TryParse(item, out num))
                {
                    reversePolishNotation.Add(item);
                }
                // Оператор
                else 
                {
                    if (item == "(")
                    {
                        stack.Push(item);
                    }
                    else if (item == ")")
                    {
                        string sign = stack.Pop();
                        while (sign != "(")
                        {
                            reversePolishNotation.Add(sign);
                            sign = stack.Pop();
                        }
                    }
                    // Другой оператор
                    else
                    {
                        if (stack.Count > 0)
                        {
                            if (Priority(item) <= Priority(stack.Peek()))
                            {
                                reversePolishNotation.Add(stack.Pop());
                            }
                        }
                        stack.Push(item);
                    }

                }

            }

            while (stack.Count > 0)
            {
                reversePolishNotation.Add(stack.Pop());
            }

            Console.WriteLine();
            foreach (var item in reversePolishNotation)
            {
                Console.Write($"{item}, ");
            }

            return reversePolishNotation;
        }

        public static int Priority(string oper)
        {
            switch (oper)
            {
                case "(": return 0;
                case ")": return 1;
                case "+": return 2;
                case "-": return 3;
                case "*": return 4;
                case "/": return 4;
                case "^": return 5;
                case "sqrt": return 5;
                default: return 6;
            }
        }

        public static double Calculating(ref string expression)
        {
            double number;

            List<string> expList = new List<string>();
            expList = RPN(expression);

            Stack<double> stack = new Stack<double>();
            foreach (string item in expList)
            {
                if (double.TryParse(item, out number))
                {
                    stack.Push(number);
                }
                else
                {
                    switch (item)
                    {
                        case "+":
                            {
                                double number1 = stack.Pop();
                                double number2 = stack.Pop();
                                stack.Push(number2 + number1);
                                break;
                            }
                        case "-":
                            {
                                double num1 = stack.Pop();
                                double num2 = stack.Pop();
                                stack.Push(num2 - num1);
                                break;
                            }
                        case "*":
                            {
                                double num1 = stack.Pop();
                                double num2 = stack.Pop();
                                stack.Push(num2 * num1);
                                break;
                            }
                        case "/":
                            {
                                double num1 = stack.Pop();
                                double num2 = stack.Pop();
                                stack.Push(num2 / num1);
                                break;
                            }
                        case "^":
                            {
                                double num1 = stack.Pop();
                                double num2 = stack.Pop();
                                stack.Push(Math.Pow(num2, num1));
                                break;
                            }
                        case "sqrt":
                            {
                                stack.Push(Math.Sqrt(stack.Pop()));
                                break;
                            }
                    }
                }

            }
            return stack.Peek();
        }

        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;

            Console.Write("Введите выражение(разделяйте пробелами): ");
            string expression = Console.ReadLine();

            double result = Calculating(ref expression);

            Console.WriteLine($"\n\nРезультат: {result}");

        }
    }
}

