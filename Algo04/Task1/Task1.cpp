﻿#include <stdio.h>
#include <stdlib.h>
#include "clist.h"

void printNode(elemtype* value)
{
    printf("%d\n", *((int*)value));
}

int main()
{
    elemtype a = 0;
    elemtype b = 1;
    elemtype c = 2;
    elemtype d = 8;
    elemtype tmp;

    cList* mylist = createList();

    pushFront(mylist, &a);
    printList(mylist, printNode);
    printf("\n");

    popBack(mylist, &tmp);

    pushFront(mylist, &b);
    printList(mylist, printNode);
    printf("\n");

    pushFront(mylist, &a);
    printList(mylist, printNode);
    printf("\n");

    pushBack(mylist, &c);
    printList(mylist, printNode);
    printf("\n");

    popFront(mylist, &tmp);
    printList(mylist, printNode);
    printf("\n");

    pushBack(mylist, &a);
    printList(mylist, printNode);
    printf("\n");

    pushFront(mylist, &a);
    printList(mylist, printNode);
    printf("\n");

    pushIndex(mylist, &d, 2);
    printList(mylist, printNode);
    printf("\n");

    popIndex(mylist, 2);
    printList(mylist, printNode);
    printf("\n");

    printNode(getNode(mylist, 3)->value);
    printf("\n");

    printList(mylist, printNode);
    printf("\n");

    deleteList(mylist);

    system("pause");
    return 0;
}
