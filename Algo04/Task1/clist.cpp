#include "clist.h"

// �������� ������� ������
cList* createList(void)
{
    cList* list = (cList*)malloc(sizeof(cList));
    if (list)
    {
        list->size = 0;
        list->head = list->tail = NULL;
    }
    return list;
}

// �������� ������
void deleteList(cList* list)
{
    cNode* head = list->head;
    cNode* next = NULL;
    while (head)
    {
        next = head->next;
        free(head);
        head = next;
    }
    free(list);
    list = NULL;
}

// �������� ������ �� �������
bool isEmptyList(cList* list)
{
    return ((list->head == NULL) || (list->tail == NULL));
}

// ���������� ������ ���� � ������ ������
int pushFront(cList* list, elemtype* data)
{
    cNode* node = (cNode*)malloc(sizeof(cNode));
    if (!node)
    {
        return(-1);
    }
    node->value = data;
    node->next = list->head;
    node->prev = NULL;

    if (!isEmptyList(list))
    {
        list->head->prev = node;
    }
    else
    {
        list->tail = node;
    }
    list->head = node;

    list->size++;
    return(0);
}

// ���������� ���� �� ������ ������
int popFront(cList* list, elemtype* data)
{
    cNode* node;

    if (isEmptyList(list))
    {
        return(-2);
    }

    node = list->head;
    list->head = list->head->next;

    if (!isEmptyList(list))
    {
        list->head->prev = NULL;
    }
    else
    {
        list->tail = NULL;
    }

    data = node->value;
    list->size--;
    free(node);

    return(0);
}

// ���������� ������ ���� � ����� ������
int pushBack(cList* list, elemtype* data)
{
    cNode* node = (cNode*)malloc(sizeof(cNode));
    if (!node)
    {
        return(-3);
    }

    node->value = data;
    node->next = NULL;
    node->prev = list->tail;
    if (!isEmptyList(list))
    {
        list->tail->next = node;
    }
    else
    {
        list->head = node;
    }
    list->tail = node;

    list->size++;
    return(0);
}

// ���������� ���� �� ����� ������
int popBack(cList* list, elemtype* data)
{
    cNode* node = NULL;

    if (isEmptyList(list))
    {
        return(-4);
    }

    node = list->tail;
    list->tail = list->tail->prev;
    if (!isEmptyList(list))
    {
        list->tail->next = NULL;
    }
    else
    {
        list->head = NULL;
    }

    data = node->value;
    list->size--;
    free(node);

    return(0);
}

// ���������� ������ ���� � ������������ �����
int pushIndex(cList* list, elemtype* data, int index)
{
    cNode* node = (cNode*)malloc(sizeof(cNode));

    if (index == 0)
    {
        return pushFront(list, data);
    }
    if (index == list->size - 1)
    {
        return pushBack(list, data);
    }
    if (index < 0 || index>list->size - 1)
    {
        return -1;
    }

    cNode* next = getNode(list, index);
    cNode* prev = getNode(list, index)->prev;

    node->value = data;

    // ��������� ���� ���� ��������� ����� ��������
    getNode(list, index - 1)->next = node;
    getNode(list, index)->prev = node;

    // ������ ���������
    node->next = next;
    node->prev = prev;

    list->size++;
}

// ���������� ���� � � ������������� �����
int popIndex(cList* list, int index)
{
    if (index == 0)
    {
        elemtype tmp;
        return popFront(list, &tmp);
    }
    if (index == list->size - 1)
    {
        elemtype tmp;
        return popBack(list, &tmp);
    }
    if (index < 0 || index>list->size - 1)
    {
        return -1;
    }

    // ���������� ����
    cNode* next = getNode(list, index)->next;
    cNode* prev = getNode(list, index)->prev;

    // ����������� ��������� ����
    free(getNode(list, index));

    // ��������� ����
    getNode(list, index - 1)->next = next;
    getNode(list, index + 1)->prev = prev;

    list->size--;

    return 0;
}

// ������ ������������� ���� ������
cNode* getNode(cList* list, int index)
{
    cNode* node = NULL;
    int i;

    if (index >= list->size)
    {
        return (NULL);
    }

    if (index < list->size / 2)
    {
        i = 0;
        node = list->head;
        while (node && i < index)
        {
            node = node->next;
            i++;
        }
    }
    else
    {
        i = list->size - 1;
        node = list->tail;
        while (node && i > index)
        {
            node = node->prev;
            i--;
        }
    }

    return node;
}

// ����� ������ � �������
void printList(cList* list, void (*func)(elemtype*))
{
    cNode* node = list->head;

    if (isEmptyList(list))
    {
        return;
    }

    while (node)
    {
        func(node->value);
        node = node->next;
    }
}
