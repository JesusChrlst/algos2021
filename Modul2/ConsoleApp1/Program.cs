﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp1
{
    class Program
    {
        private char Symbol;
        private double Probability;
        private string Binary = "";

        public static void FindProbability(char[] letters, ref List<Program> listShannonFano)
        {
            //для букв
            List<char> repeatedLetters = new List<char>();
            //к-во повторений букв
            int[] repeatedLettersCount = new int[letters.Length];
            int countCheck = -1;

            for (int i = 0; i < letters.Length; i++)
            {
                //пропуск если буква уже есть
                if (repeatedLetters.Contains(letters[i]))
                {
                    continue;
                }
                repeatedLetters.Add(letters[i]);
                countCheck++;
                //подсчёт буквы
                for (int j = 0; j < letters.Length; j++)
                {
                    if (letters[i] == letters[j])
                    {
                        repeatedLettersCount[countCheck]++;
                    }
                }
            }

            double value = 0;

            for (int i = 0; i < repeatedLetters.Count; i++)
            {
                value = Math.Round(repeatedLettersCount[i] / (double)letters.Length, 4);
                listShannonFano.Add(new Program() { Symbol = repeatedLetters[i], Probability = value });
            }

            //Сортировка
            listShannonFano = listShannonFano.OrderBy(x => x.Probability).ToList();
            listShannonFano.Reverse();


            Console.WriteLine();
            foreach (var keyValue in listShannonFano)
            {
                Console.WriteLine($"Вероятность появления {keyValue.Symbol} = {keyValue.Probability}");
            }
        }

        public static void ShannonFano(int L, int R, ref List<Program> listShannonFano)
        {
            int n;

            if (L < R)
            {
                //находим максимально близкие вероятности для 2 узлов(групп)
                n = DivideSequence(L, R, ref listShannonFano);
                //разделяем узлы
                for (int i = L; i <= R; i++)
                {
                    //для прохода по левой части дерева
                    //сначала для всех значений что слева присваиваем по 1
                    if (i <= n)
                    {
                        listShannonFano[i].Binary += "1";
                    }
                    //всем значениям справа - 0
                    else
                    {
                        listShannonFano[i].Binary += "0";
                    }
                }
                //повторяем функцию до тех пор, пока R не станет 0, проход по левой ветке
                ShannonFano(L, n, ref listShannonFano);
                //Смещаем L для прохода по остальной части дерева чтобы проинициализировать остальные элементы
                ShannonFano(n + 1, R, ref listShannonFano);
            }
        }


        public static int DivideSequence(int L, int R, ref List<Program> listShannonFano)
        {
            int j;
            double countLeft = 0, countRight = 0;

            //проходимся по вероятностям с начала
            for (int i = L; i <= R - 1; i++)
            {
                countLeft += listShannonFano[i].Probability;
            }

            //инициализируем последнюю вероятность
            countRight = listShannonFano[R].Probability;

            //для прохода с конца
            j = R;
            while (countLeft >= countRight)
            {
                //уменьшаем count1 одновременно увеличивая count2 для выравнивания вероятностей в групках
                j--;
                countLeft -= listShannonFano[j].Probability;
                countRight += listShannonFano[j].Probability;
            }
            return j;
            //j отвечает за индекс, после которого куча будет разбита на 2 группы
        }

        public static string ReverseString(string rev)
        {
            return new string(rev.Reverse().ToArray());
        }

        public static void Main()
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.Write("Введите строку(тестовые данные): ");
            string str = Console.ReadLine();

            char[] letters = new char[str.Length];
            letters = str.ToCharArray();

            List<Program> listShannonFano = new List<Program>();

            FindProbability(letters, ref listShannonFano);
            ShannonFano(0, listShannonFano.Count - 1, ref listShannonFano);

            Console.WriteLine();
            foreach (var item in listShannonFano)
            {
                item.Binary = ReverseString(item.Binary);
                Console.WriteLine($"{item.Symbol} = {item.Binary}");
            }
        }
    }
}